use crate::common::jwt;
use jsonwebtoken::{decode, encode, Algorithm, DecodingKey, EncodingKey, Header, Validation};

pub static JWT_SECRET: &'static str = "seecreet";

/// Encodes a JWT token.
///
/// Takes in the authorized user id and expiry date of the authorization.
/// Outputs a string of the token.
///
/// Arguments:
/// user_id - The DB entry id of the authorized user.
/// exp - The unix epoch at which the token expires
///
/// Returns: a string of the token on success, string on error.
///
/// Example:
/// ```rust
///     let token = create_jwt_token(auth_user_id, unix_expiry_epoch).unwrap();
/// ```
pub fn create_jwt_token(user_id: i64, exp: u64) -> Result<String, String> {
    let mut header = Header::default();
    header.alg = Algorithm::HS512;

    let claim = jwt::Claim {
        user_id: user_id,
        exp: exp,
    };
    match encode(
        &header,
        &claim,
        &EncodingKey::from_secret(JWT_SECRET.as_bytes()),
    ) {
        Ok(token) => Ok(token),
        Err(_) => Err("Failed to create JWT token.".to_string()),
    }
}

/// Decodes and verifies a JWT token.
///
/// Takes in an encoded JWT token and outputs it's JWTClaim.
///
/// Arguments:
/// token - the JWT token to be decoded
///
/// Returns: JWTClaim on success, nothing on error.
///
/// Example:
/// ```rust
///     assert_eq!(verify_jwt_token(token).unwrap(), true);
/// ```
pub fn verify_jwt_token(token: String) -> Result<jwt::Claim, String> {
    let mut validation = Validation::new(Algorithm::HS512);
    validation.leeway = 25;
    match decode::<jwt::Claim>(
        &token,
        &DecodingKey::from_secret(JWT_SECRET.as_bytes()),
        &validation,
    ) {
        Ok(data) => Ok(data.claims),
        Err(_) => Err("Invalid JWT token.".to_string()),
    }
}
