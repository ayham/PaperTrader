use argon2::password_hash::SaltString;
use rand_core::OsRng;

use crate::common::message::*;

use crate::server::account;
use crate::server::db::cmd::get_client_salt::*;

use tokio::net::TcpStream;
use tokio_rustls::server::TlsStream;

pub async fn handle_data(
    sql_conn: &tokio_postgres::Client,
    socket: &mut TlsStream<TcpStream>,
    buf: &[u8],
) -> Result<(), String> {
    /* decode incoming message */
    let client_msg: Message = bincode::deserialize(&buf)
        .map_err(|err| format!("HANDLE_DATA_RCVD_INVALID_MSG: {}", err))?;

    /* handle individual client instructions */
    // a command which is executed is assumed to return an IO result.
    // handle_data() would then log if writing to client failed.
    let cmd_client_write_result: std::io::Result<()> = match client_msg.command {
        Command::GenHashSalt => {
            Message::new()
                .command(Command::Success)
                .data(SaltString::generate(&mut OsRng).as_str().to_string())
                .send(socket)
                .await
        }
        Command::GetEmailSalt | Command::GetPasswordSalt => {
            let salt = get_client_salt(sql_conn, client_msg.get_data()?, client_msg.command).await;
            let mut response = Message::new();
            if salt.is_ok() {
                response
                    .command(Command::Success)
                    .data(salt.unwrap())
                    .send(socket)
                    .await
            } else {
                response.command(Command::Failure).send(socket).await
            }
        }
        Command::Register => account::create(sql_conn, socket, &client_msg).await,
        Command::LoginMethod1 => account::authorize(sql_conn, socket, &client_msg).await,
        _ => {
            Message::new()
                .command(Command::Failure)
                .data("Could not handle an unknown command!")
                .send(socket)
                .await
        }
    };

    match cmd_client_write_result {
        Ok(_) => Ok(()),
        Err(e) => Err(format!(
            "Failed running command, writing to socket failed. Error: {}",
            e
        )),
    }
}
