use crate::common::account::portfolio::Portfolio;
use crate::common::account::transaction::Transaction;

#[derive(PartialEq, Debug)]
pub struct Account {
    pub username: String,

    pub email_hash_phc: String,
    pub client_email_salt: String,

    pub passw_hash_phc: String,
    pub client_passw_salt: String,

    pub portfolio: Portfolio,
    pub transactions: Vec<Transaction>,
}

impl std::fmt::Display for Account {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "({}, {}, {}, {}, {}, {}, {:#?})",
            self.username,
            self.email_hash_phc,
            self.client_email_salt,
            self.passw_hash_phc,
            self.client_passw_salt,
            self.portfolio,
            self.transactions
        )
    }
}
