use argon2::{
    password_hash::{PasswordHasher, SaltString},
    Argon2,
};
use rand_core::OsRng;

/// Generates a storable server email hash from a client hashed email.
///
/// Takes in a client hashed email, outputs a storable new hash. The returned result is 'safe' to
/// be stored on the server side database. The salt returned is for the hashed version of the
/// hashed client email.
///
/// Arguments:
/// hashed_email - The client hashed email sent to the server.
///
/// Returns: a tuple containing the final hash and the hash's salt, nothing on failure.
///
/// Example:
/// ```rust
///     let enc = hash_email("THISISTOTALLYAHASHEDTHING...").unwrap();
///     println!("Server Email Hash: {}", HEXUPPER.encode(&enc.0));
///     println!("Server Email Salt: {}", HEXUPPER.encode(&enc.1));
/// ```
pub fn hash(hashed_email: String) -> String {
    let salt = SaltString::generate(&mut OsRng);

    let argon2id = Argon2::default();
    let phc = argon2id
        .hash_password_simple(&hashed_email.as_bytes(), &salt)
        .unwrap();

    phc.to_string()
}
