use log::warn;

use argon2::{
    password_hash::{PasswordHash, PasswordVerifier},
    Argon2,
};

use crate::common::message::*;

use crate::server::db::cmd::get_user_hash::*;
use crate::server::db::cmd::get_user_id::*;

use crate::server::network::jwt::*;

use tokio::io::AsyncWriteExt;
use tokio::net::TcpStream;
use tokio_rustls::server::TlsStream;

pub async fn authorize(
    sql_conn: &tokio_postgres::Client,
    socket: &mut TlsStream<TcpStream>,
    message: &Message,
) -> std::io::Result<()> {
    /* assert recieved message */
    if !message.assert_command(Command::LoginMethod1) || !message.assert_data() {
        warn!("LOGIN_INVALID_MESSAGE");
        return socket.shutdown().await;
    }

    /*
     * Parse account data.
     * */
    /* get json data */
    let stringified_data: String = message.get_data().map_err(|_| {
        std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            "Failed authorizing account, could not parse message data!",
        )
    })?;
    let data = json::parse(&stringified_data).unwrap();
    /* get email, password, and username hashes */
    let email_client_hash = data["hashed_email"].as_str().unwrap();
    let passw_client_hash = data["hashed_passw"].as_str().unwrap();
    let username = data["username"].as_str().unwrap();

    /*
     * Get server hashes
     * */
    let email_server_hash = match get_user_hash(sql_conn, username, true).await {
        Ok(val) => val,
        Err(_) => {
            return Message::new()
                .command(Command::Failure)
                .data(format!(
                    "Failed authorizing user, {}, does not exist!",
                    username
                ))
                .send(socket)
                .await;
        }
    };

    let passw_server_hash = match get_user_hash(sql_conn, username, false).await {
        Ok(val) => val,
        Err(_) => {
            return Message::new()
                .command(Command::Failure)
                .data(format!(
                    "Failed authorizing user, {}, does not exist!",
                    username
                ))
                .send(socket)
                .await;
        }
    };

    /*
     * Verify creds
     * */
    let argon2id = Argon2::default();

    let parsed_email_hash = match PasswordHash::new(&email_server_hash) {
        Ok(val) => val,
        Err(_) => {
            return Message::new()
                .command(Command::Failure)
                .data("Email Hash Invalid")
                .send(socket)
                .await;
        }
    };
    if argon2id
        .verify_password(&email_client_hash.as_bytes(), &parsed_email_hash)
        .is_err()
    {
        return Message::new()
            .command(Command::Failure)
            .data("Email Incorrect")
            .send(socket)
            .await;
    }

    let parsed_passw_hash = match PasswordHash::new(&passw_server_hash) {
        Ok(val) => val,
        Err(_) => {
            return Message::new()
                .command(Command::Failure)
                .data("Password Hash Invalid")
                .send(socket)
                .await;
        }
    };
    if argon2id
        .verify_password(&passw_client_hash.as_bytes(), &parsed_passw_hash)
        .is_err()
    {
        return Message::new()
            .command(Command::Failure)
            .data("Password Incorrect")
            .send(socket)
            .await;
    }

    /*
     * Generate JWT token
     * */
    /* get user id*/
    let user_id = match get_user_id(sql_conn, username).await {
        Ok(val) => val,
        Err(_) => {
            return Message::new()
                .command(Command::Failure)
                .data(format!(
                    "Failed authorizing user, {}, does not exist!",
                    username
                ))
                .send(socket)
                .await;
        }
    };

    /* gen the actual token */
    use std::time::{Duration, SystemTime, UNIX_EPOCH};
    let beginning_of_time = SystemTime::now() + Duration::from_secs(4 * 60 * 60);

    let jwt_result = create_jwt_token(
        user_id,
        beginning_of_time
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_secs(),
    )
    .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))?;

    Message::new()
        .command(Command::Success)
        .data(jwt_result)
        .send(socket)
        .await
}
