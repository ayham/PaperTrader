use argon2::password_hash::PasswordHash;
use log::warn;

use crate::common::account::portfolio::*;
use crate::common::message::*;

use crate::server::account::hash::*;

use crate::server::db::cmd::user_exists::*;
use crate::server::ds::account::*;

use tokio::io::AsyncWriteExt;
use tokio::net::TcpStream;
use tokio_rustls::server::TlsStream;

pub async fn create(
    sql_conn: &tokio_postgres::Client,
    socket: &mut TlsStream<TcpStream>,
    message: &Message,
) -> std::io::Result<()> {
    /* assert recieved message */
    if !message.assert_command(Command::Register) || !message.assert_data() {
        warn!("REGISTER_INVALID_MESSAGE");
        return socket.shutdown().await; // just get off of my lawn
    }
    /*
     * Parse account data
     * */
    /* get json data */
    let stringified_data: String = bincode::deserialize(&message.data).unwrap();
    let data = json::parse(&stringified_data).unwrap();

    /*
     * check if username is available in the database
     * */
    let username = data["username"].as_str().unwrap();

    /* search for an account with same name */
    if !user_exists(sql_conn, username).await {
        /*
         * Inform cient that user already exists
         * Note: figure out if this is a security? issue
         */
        return Message::new()
            .command(Command::Failure)
            .data("Username already exists")
            .send(socket)
            .await;
    }

    /* get email, password client PHC strings */
    let (email_client_hash_phc, passw_client_hash_phc) = match (
        data["email_client_hash_phc"].as_str(),
        data["passw_client_hash_phc"].as_str(),
    ) {
        (Some(a), Some(b)) => (a, b),
        _ => {
            /* received empty PHC strings */
            return Message::new()
                .command(Command::Failure)
                .data("Received empty PHC strings")
                .send(socket)
                .await;
        }
    };

    let (email_client, passw_client) = match (
        PasswordHash::new(email_client_hash_phc),
        PasswordHash::new(passw_client_hash_phc),
    ) {
        (Ok(a), Ok(b)) => (a, b),
        _ => {
            return Message::new()
                .command(Command::Failure)
                .data("Received invalid PHC strings")
                .send(socket)
                .await;
        }
    };

    /* store salt _ONLY_ from the PHC string received,
     * Note: discard the main hash sent from client after hashing by server */
    let (email_client_salt, passw_client_salt) = match (email_client.salt, passw_client.salt) {
        (Some(a), Some(b)) => (a, b),
        _ => {
            return Message::new()
                .command(Command::Failure)
                .data("Received invalid salts")
                .send(socket)
                .await;
        }
    };

    /* generate account struct */
    let mut account: Account = Account {
        username: username.to_string(),

        email_hash_phc: "".to_string(),
        client_email_salt: email_client_salt.to_string(),

        passw_hash_phc: "".to_string(),
        client_passw_salt: passw_client_salt.to_string(),

        portfolio: Portfolio::default(),
        transactions: Vec::new(),
    };

    /*
     * Hash the email and password and store them.
     * */
    if let (Some(email_client_hash), Some(passw_client_hash)) =
        (email_client.hash, passw_client.hash)
    {
        account.email_hash_phc = hash(email_client_hash.to_string());
        account.passw_hash_phc = hash(passw_client_hash.to_string());
    } else {
        return Message::new()
            .command(Command::Failure)
            .data("Received empty hashes")
            .send(socket)
            .await;
    }

    /*
     * Write the account to the database.
     * */
    let creation_result = sql_conn
        .execute(
            "INSERT INTO accounts_schema.accounts \
        (username, email_hash_phc, client_email_salt, pass_hash_phc, client_pass_salt)
        VALUES \
        ($1, $2, $3, $4, $5)",
            &[
                &account.username,
                &account.email_hash_phc,
                &account.client_email_salt,
                &account.passw_hash_phc,
                &account.client_passw_salt,
            ],
        )
        .await;

    /*
     * Send to client SQL result
     */
    match creation_result {
        Ok(_) => Message::new().command(Command::Success).send(socket).await,
        Err(_) => {
            Message::new()
                .command(Command::Failure)
                .data(format!(
                    "Failed creating an account, server error, {:#?}, \
                              please try again later.",
                    creation_result
                ))
                .send(socket)
                .await
        }
    }
}
