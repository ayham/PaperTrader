use crate::common::command::*;
use crate::server::db::cmd::user_exists::*;

pub async fn get_client_salt(
    sql_conn: &tokio_postgres::Client,
    username: &str,
    command: Command,
) -> Result<String, String> {
    /* check that user exists */
    if user_exists(sql_conn, username).await {
        let query_variable: String = match command {
            Command::GetEmailSalt => "client_email_salt",
            Command::GetPasswordSalt => "client_pass_salt",
            _ => {
                return Err(format!(
                    "Could not get salt for user, {}, requested salt type, {}, is invalid.",
                    username, command
                ))
            }
        }
        .into();

        for row in &sql_conn
            .query(
                format!(
                    "SELECT username, {} FROM accounts_schema.accounts WHERE username LIKE $1",
                    &query_variable
                )
                .as_str(),
                &[&username],
            )
            .await
            .unwrap()
        {
            return Ok(row.get(1));
        }
    }

    Err("Failed to retrieve salt of non-existing user.".to_string())
}
