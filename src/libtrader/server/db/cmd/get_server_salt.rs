use argon2::password_hash::PasswordHash;

use crate::common::command::*;
use crate::server::db::cmd::user_exists::*;

pub async fn get_server_salt(
    sql_conn: &tokio_postgres::Client,
    username: &str,
    command: Command,
) -> Result<String, String> {
    /* check that user exists */
    if user_exists(sql_conn, username).await {
        let query_variable: String = match command {
            Command::GetEmailSalt => "email_hash_phc",
            Command::GetPasswordSalt => "pass_hash_phc",
            _ => {
                return Err(format!(
                    "Could not get salt for user, {}, requested salt type, {}, is invalid.",
                    username, command
                ))
            }
        }
        .into();

        for row in &sql_conn
            .query(
                format!(
                    "SELECT username, {} FROM accounts_schema.accounts WHERE username LIKE $1",
                    query_variable
                )
                .as_str(),
                &[&username],
            )
            .await
            .unwrap()
        {
            /* parse PHC string */
            let parsed_phc = PasswordHash::new(row.get(1))
                .map_err(|_| format!("Account, {}, corrupted, invalid parsed PHC", username))?;
            if let Some(salt) = parsed_phc.salt {
                return Ok(salt.to_string());
            } else {
                return Err(format!("Account, {}, corrupted, empty salt!", username));
            }
        }
    }

    Err("Failed to retrieve salt of non-existing user.".to_string())
}
