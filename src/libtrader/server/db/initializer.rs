/// Establishes a postgresql connection to the SQL database.
///
/// Creates a postgresql connection.
/// Should be used in Async contexts.
///
/// Arguments:
/// user - The name of the user to connect to the database with.
/// pass - The password of the user to connect to the database with.
///
/// Returns: ```Result``` wrapping  ```tokio_postgres::Client``` on success,
/// and ```tokio_postgres::Error``` on error.
///
/// Example:
/// ```rust
/// let mut client = db_connect(DB_USER, DB_PASS)?;
/// ```
pub async fn db_connect(user: String, pass: String) -> std::io::Result<tokio_postgres::Client> {
    /* Generate the requested string */
    let db_connect_str = format!(
        "host={} port={} dbname={} user={} password={}",
        std::env::var("DB_HOST").unwrap(),
        std::env::var("DB_HOST_PORT").unwrap(),
        std::env::var("DB_NAME").unwrap(),
        user,
        pass
    );
    let (client, connection) =
        tokio_postgres::connect(db_connect_str.as_str(), tokio_postgres::NoTls)
            .await
            .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, format!("{}", err)))?;
    tokio::spawn(async move {
        if let Err(e) = connection.await {
            eprintln!("SQL connection error: {}", e);
        }
    });
    Ok(client)
}
