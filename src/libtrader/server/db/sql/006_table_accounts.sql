CREATE TABLE accounts_schema.accounts (
	id				  BIGSERIAL PRIMARY KEY,
	username		  TEXT UNIQUE NOT NULL,

	email_hash_phc	  TEXT UNIQUE NOT NULL,
	client_email_salt TEXT UNIQUE NOT NULL,

	pass_hash_phc	  TEXT UNIQUE NOT NULL,
	client_pass_salt  TEXT UNIQUE NOT NULL
)
