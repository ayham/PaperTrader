use std::io;

use crate::common::message::*;

use crate::client::account::hash::*;

use crate::client::network::cmd::get_server_salt::get_server_salt;

use tokio::net::TcpStream;
use tokio_rustls::client::TlsStream;

/// Requests a TLS server to create an account.
///
/// Gets three server salts, generates three new salts, cancatenates both salts, and use the
/// concatenated salt to hash the username, email, and password. Generates a message containing the
/// hashes and sends it to the server. Waits for a response and returns.
/// Should be used in contexts that return ```io:;Result```.
/// Should be used in Async contexts.
///
/// Arguments:
/// socket - The TLS socket to use.
/// username - The username to send to the server.
/// email - The email to send to the server.
/// password - The password to send to the server.
///
/// Returns: ```io::Result``` indicating success or failure.
///
/// Example:
/// ```rust
///     match acc_create(&mut tlsclient, &mut  poll, "test", "test", "test") {
///         Ok(()) => println!("server returned yes"),
///         Err(err) => panic!("panik {}", err),
///     }
/// ```
pub async fn create(
    socket: &mut TlsStream<TcpStream>,
    username: &str,
    email: &str,
    passw: &str,
) -> std::io::Result<()> {
    /*
     * get two server salts for email, and password
     * */
    let email_server_salt = get_server_salt(socket).await?;
    let passw_server_salt = get_server_salt(socket).await?;

    /*
     * generate hashes for email, password
     * */
    let email_hash_phc = hash(email, &email_server_salt, true);
    let passw_hash_phc = hash(passw, &passw_server_salt, true);

    /* generate message to be sent to the server */
    let data = object! {
        email_client_hash_phc: email_hash_phc,
        passw_client_hash_phc: passw_hash_phc,
        username: username
    };
    /* build message request */
    Message::new()
        .command(Command::Register)
        .data(data.to_string())
        .send(socket)
        .await?;

    /* decode response */
    let ret_msg = Message::receive(socket).await?;

    /* assert received message */
    if !ret_msg.assert_command(Command::Success) || !ret_msg.assert_data() {
        /* created successfully */
        return Ok(());
    } else {
        /* server rejected account creation */
        return Err(io::Error::new(
            io::ErrorKind::ConnectionRefused,
            format!(
                "Failed creating account for user, {}, server reason: {}",
                username,
                ret_msg.get_err()
            ),
        ));
    }
}
