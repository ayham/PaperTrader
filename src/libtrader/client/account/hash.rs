use argon2::{
    password_hash::{PasswordHasher, SaltString},
    Argon2,
};
use rand_core::OsRng;

/// Generates a client email hash from a raw email.
///
/// Takes in a raw email, outputs a hashed version of the client email to be sent to the server
/// with the returned client random bits that make up the whole client salt. This function is to be
/// used on client random bits that make up the whole client salt. This function is to be used on
/// client  side account creation. The result from this function is not be stored directly on the
/// database, result must be run through the server side hashing again.
///
/// Arguments:
/// email - The raw user email to be hashed.
/// server_salt - The server's part sent of the salt.
///
/// Returns: a tuple containing the client hash and full salt, nothing on failure.
///
/// Example:
/// ```rust
///     let enc = hash_email("totallyrealemail@anemail.c0m", server_salt).unwrap();
/// ```
pub fn hash(email: &str, server_salt: &SaltString, resalt: bool) -> String {
    let mut raw_salt: String = server_salt.as_str().to_string();
    if resalt {
        raw_salt.push_str(SaltString::generate(&mut OsRng).as_str());
    }

    let salt = SaltString::new(&raw_salt).unwrap();

    let argon2id = Argon2::default();
    let phc = argon2id
        .hash_password_simple(&email.as_bytes(), &salt)
        .unwrap();

    phc.to_string()
}
