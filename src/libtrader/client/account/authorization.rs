use std::io;

use argon2::password_hash::PasswordHash;

use crate::common::message::*;

use crate::client::account::hash::*;
use crate::client::network::cmd::req_server_salt::*;

use tokio::net::TcpStream;
use tokio_rustls::client::TlsStream;

/// Client authentication procedure.
///
/// Takes in the username, email and password. Data is hashed and then sent to the server for
/// further hashing and confirmation of authentication. A session token is returned.
/// The function is not complete.
/// Should be used in contexts that return ```io::Result```.
/// Should be used in Async contexts.
///
/// Arguments:
/// socket - TLS socket to use.
/// username - The raw username to be used.
/// email - The raw email to be used.
/// password - The raw password to be used.
///
/// Returns: ```io::Result``` wraps JWT token on succes, and contains error message on failure.
///
/// Example:
/// ```rust
///     match acc_auth(&mut socket, "username", "email", "password").await {
///         Ok(jwt) => { /* use JWT token */ }
///         Err(err) => panic!("panik! {}", err), /* unauth OR invalid JWT token */
///     }
/// ```
pub async fn authorize(
    socket: &mut TlsStream<TcpStream>,
    username: &str,
    email: &str,
    passw: &str,
) -> std::io::Result<String> {
    /*
     * get email salt
     * */
    let email_salt = req_server_salt(socket, username, Command::GetEmailSalt).await?;

    /*
     * get password salt
     * */
    let passw_salt = req_server_salt(socket, username, Command::GetPasswordSalt).await?;

    /*
     * hash the email
     */
    let hashed_email = hash(email, &email_salt, false);

    /*
     * hash the password
     */
    let hashed_passw = hash(passw, &passw_salt, false);

    /*
     * only send hash without other PHC information
     * */
    let parsed_email = PasswordHash::new(&hashed_email).unwrap();
    let parsed_passw = PasswordHash::new(&hashed_passw).unwrap();
    let parsed_email_hash = parsed_email.hash.unwrap().to_string();
    let parsed_passw_hash = parsed_passw.hash.unwrap().to_string();

    /* generate message to be sent to the server */
    let data = object! {
        hashed_email: parsed_email_hash,
        hashed_passw: parsed_passw_hash,
        username: username
    };
    /* build message request */
    Message::new()
        .command(Command::LoginMethod1)
        .data(data.to_string())
        .send(socket)
        .await?;

    /* decode response */
    let ret_msg = Message::receive(socket).await?;

    /* decode response */
    if !ret_msg.assert_command(Command::Success) || !ret_msg.assert_data() {
        Err(io::Error::new(
            io::ErrorKind::ConnectionRefused,
            format!(
                "Failed authorizing account, {}, server returned error: {}.",
                username,
                ret_msg.get_err()
            ),
        ))
    } else {
        /* authorized */
        Ok(ret_msg.get_data().map_err(|_| {
            io::Error::new(
                io::ErrorKind::InvalidData,
                format!(
                    "Failed authorizing account, {}, server returned invalid data.",
                    username
                ),
            )
        })?)
    }
}
