use serde::{Deserialize, Serialize};

pub use crate::common::account::position::*;

#[derive(Serialize, Deserialize, PartialEq, Debug, Default)]
pub struct Portfolio {
    pub open_positions: Vec<Position>,
}

impl std::fmt::Display for Portfolio {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({:#?})", self.open_positions)
    }
}
