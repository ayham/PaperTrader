use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone, Copy)]
pub enum Command {
    Default = 0,
    Success = 1,
    Failure = 2,
    LoginMethod1 = 3,
    LoginMethod2,
    Register,
    PurchaseAsset,
    SellAsset,
    GenHashSalt,
    GetEmailSalt,
    GetPasswordSalt,
    GetAssetInfo,
    GetAssetValue,
    GetAssetValueCurrent,
    GetUserInfo,
    GetUserPortfolio,
    GetUserTransactionHist,
}
impl std::fmt::Display for Command {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:#?}", self)
    }
}

impl Default for Command {
    fn default() -> Self {
        Command::Success
    }
}
