#!/bin/sh

. ./scripts/env.sh && cargo run --no-default-features --features "server" -- ${1:-0.0.0.0:4000}
