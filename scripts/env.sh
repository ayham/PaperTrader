#!/bin/sh

export CONFIG_CERT_FILE="./certs/certificate.crt"
export CONFIG_KEY_FILE="./certs/private.key"

export DB_HOST="database"
[ -z "$CI_DEPLOY" ] && export DB_HOST="localhost"

export DB_HOST_PORT="5432"
export DB_NAME="pt_db"

export DB_USER="pt_usr"
export DB_PASS="PASSWORD"

export DB_SESS_USER="sessions_schema_usr"
export DB_SESS_PASS="PASSWORD"

export DB_ACC_USER="accounts_schema_usr"
export DB_ACC_PASS="PASSWORD"

export DB_PORTFOLIO_USER="portfolio_schema_usr"
export DB_PORTFOLIO_PASS="PASSWORD"
