#!/bin/sh

cargo run --no-default-features  --features "client,tls_no_verify" -- ${1:-0.0.0.0:4000}
