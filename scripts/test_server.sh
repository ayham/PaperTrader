#!/bin/sh

. ./scripts/env.sh && cargo run --no-default-features --features "server" -- ${1:-0.0.0.0:4000} &

PID_server=$!
echo $PID_server

while kill -0 $PID_server > /dev/null 2>&1 
do
		[[ -f "/tmp/paper/running" ]] && break
done

if [[ -f "/tmp/paper/running" ]]; then
		cargo test --no-default-features --features "client,tls_no_verify"
		rm /tmp/paper/running
		kill $PID_server
		pkill sandbox
fi
